import React, {Component} from 'react';
import Page from './Page';

/**
 *
 */
export default class Content extends Component{
	/**
	 * [render description]
	 * @return {[type]} [description]
	 */
	render(){
		return (<Page path={this.props.path} />);
	}
};
