import React, {Component} from 'react';
import WPAPI from 'wpapi';
import { Link, NavLink } from 'react-router-dom';
const wp = new WPAPI({ endpoint: 'http://192.168.1.144/wordpress/wp-json/' });
wp.menus = wp.registerRoute('wp-api-menus/v2', '/menu-locations/(?P<location>)');

/**
 * [state description]
 * @type {Object}
 */
export default class Header extends Component{
	/**
	 * [render description]
	 * @return {[type]} [description]
	 */
	render(){
		return (
			<header>
				<div className="container clearfix">
					<div id="site-title"><Link to="/">Site</Link></div>
					<nav role="navigation">
						<Menu />
					</nav>
				</div>
			</header>
		);
	}
}

/**
 * [Stuff description]
 */
class Menu extends Component{
	/**
	 * [constructor description]
	 * @param  {[type]} props [description]
	 * @return {[type]}       [description]
	 */
	constructor(props){
		super(props);
		this.state = {
			items: this.props.items
		};
	}

	/**
	 * [expandList description]
	 * @param  {[type]} data [description]
	 * @return {[type]}      [description]
	 */
	expandList(data){
		return data.map((item) => {
			return (<li key={item.ID}><NavLink to={item.url.replace('http://192.168.1.144/wordpress/', '/')} activeClassName="active">{item.title}</NavLink>
				{
					(item.children.length)
						? <ul>{this.expandList(item.children)}</ul> : ''
				}
			</li>);
		});
	}

	/**
	 * [componentDidMount description]
	 * @return {[type]} [description]
	 */
	componentDidMount(){
		wp.menus().location('main_navigation').get((err, data) => {
			if(err){
				console.log(err);
			}
			this.setState({
				items: this.expandList(data)
			});
		});
	}

	/**
	 * [render description]
	 * @return {[type]} [description]
	 */
	render(){
		return (
			<ul>
				{this.state.items}
			</ul>
		);
	}
}
