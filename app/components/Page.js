import React, {Component} from 'react';
import WPAPI from 'wpapi';
import Async from 'react-promise';
const wp = new WPAPI({ endpoint: 'http://192.168.1.144/wordpress/wp-json/' });
/**
 *
 */
export default class Page extends Component{
	/**
	 * [constructor description]
	 * @param  {[type]} props [description]
	 * @return {[type]}       [description]
	 */
	constructor(props){
		super(props);
		this.state = {
			content: this.props.content,
			title: this.props.title
		};
		this.loadContent = this.loadContent.bind(this);
	}

	/**
	 * [loadContent description]
	 * @return {[type]} [description]
	 */
	loadContent(props = this.props){
		wp.pages().slug(props.path === '/' ? 'home' : props.path.match(/([/][^/]+)?([/])$/i)[0]).get((err, data) => {
			if(err){
				console.log(err);
			}
			this.setState({
				content: data[0] ? data[0].content.rendered : '',
				title: data[0] ? data[0].title.rendered : ''
			});
		});
	}

	/**
	 * [componentDidMount description]
	 * @return {[type]} [description]
	 */
	componentDidMount(){
		this.loadContent();
	}

	/**
	 * [componentWillReceiveProps description]
	 * @return {[type]} [description]
	 */
	componentWillReceiveProps(nextProps){
		this.loadContent(nextProps);
	}

	/**
	 * [render description]
	 * @return {[type]} [description]
	 */
	render(){
		return (
			<div id="main-content">
				<h1 dangerouslySetInnerHTML={{__html: this.state.title}} />
				<div dangerouslySetInnerHTML={{__html: this.state.content}} />
				{
					(this.props.path === '/blog/')
						? <Posts slug={this.props.path}/> : ''
				}
			</div>
		);
	}
};

/**
 *
 */
class Posts extends Component{
	/**
	 * [constructor description]
	 * @param  {[type]} props [description]
	 * @return {[type]}       [description]
	 */
	constructor(props){
		super(props);
		this.state = {
			posts: this.props.posts
		};
		this.loadPosts = this.loadPosts.bind(this);
	}

	/**
	 * [loadContent description]
	 * @return {[type]} [description]
	 */
	loadPosts(){
		wp.posts().get((err, data) => {
			if(err){
				console.log(err);
			}

			this.setState({
				posts: data.map((item) => {
					var meida = wp.media().id(item.featured_media).get((err, data) => {
						if(err){
							console.log(err);
						}
					});

					return (
						<Async key={item.id} promise={meida} then={(val) => {
							return (
								<li>
									<img src={val.media_details.sizes.medium.source_url} />
									<h3><a href={item.link.replace('192.168.1.144/wordpress/', '192.168.1.144:8080/')}>{item.title.rendered}</a></h3>
									<div dangerouslySetInnerHTML={{__html: item.excerpt.rendered}} />
								</li>
							);
						}} />
					);
				})
			});
		});
	}

	/**
	 * [componentDidMount description]
	 * @return {[type]} [description]
	 */
	componentDidMount(){
		this.loadPosts();
	}

	/**
	 * [render description]
	 * @return {[type]} [description]
	 */
	render(){
		return (
			<ul>
				{this.state.posts}
			</ul>
		);
	}
};
