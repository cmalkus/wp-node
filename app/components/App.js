import React, {Component} from 'react';
import Header from './Header';
import Content from './Content';

/**
 *
 */
export default class App extends Component{
	/**
	 * [render description]
	 * @return {[type]} [description]
	 */
	render(){
		return (
			<div>
				<Header />
				<Content path={this.props.location.pathname} />
			</div>
		);
	}
}
