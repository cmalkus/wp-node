import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Link } from 'react-router-dom';
import WPAPI from 'wpapi';
import path from 'path';
import App from './components/App';
const wp = new WPAPI({ endpoint: 'http://localhost/wordpress/wp-json/' });

ReactDOM.render(
	<BrowserRouter >
		<Route path="*" component={App}/>
	</BrowserRouter>,
	document.body
);
