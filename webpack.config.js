const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const HTMLWebpackPluginConfig = new HTMLWebpackPlugin({
	template: path.join(__dirname, '/app/index.html'),
	filename: 'index.html',
	inject: 'body'
});
module.exports = {
	entry: path.join(__dirname, '/app/index.js'),
	module: {
		loaders: [{
			test: /\.js$/,
			exclude: /node_modules/,
			loader: 'babel-loader'
		},
		{
			test: /\.jsx$/,
			exclude: /node_modules/,
			loader: 'babel-loader'
		}]
	},
	output: {
		filename: 'main.js',
		path: path.join(__dirname, '/build'),
		publicPath: '/'
	},
	devServer: {
		historyApiFallback: true
	},
	plugins: [HTMLWebpackPluginConfig]
};
